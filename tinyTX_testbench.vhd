--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   11:49:33 11/20/2017
-- Design Name:   
-- Module Name:   C:/Users/Jack Gassett/dev/tinyTX/papilio-pro_0/bld-ise/papilio_pro_tb.vhd
-- Project Name:  papilio-pro_0
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: papilio_pro
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
USE ieee.numeric_std.ALL;

use std.textio.all;
use IEEE.std_logic_textio.all;
 
ENTITY tinyTX_testbench IS
END tinyTX_testbench;
 
ARCHITECTURE behavior OF tinyTX_testbench IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT tinyTX_example
    PORT(
         clk : IN  std_logic;
         switch : IN  std_logic_vector(7 downto 0);
         JOY_UP : IN  std_logic;
         tx : OUT  std_logic;
         led : OUT  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal sw : std_logic_vector(7 downto 0) := (others => '0');
   signal btnC : std_logic := '0';

 	--Outputs
   signal RsTx, baud_clock : std_logic;
   signal led : std_logic_vector(7 downto 0);

   -- Clock period definitions
   constant clk_period : time := 31.25 ns;
	
	signal count      : unsigned(12 downto 0)        := (others => '1');
	signal ascii_count : unsigned(7 downto 0)	:= x"41";
	signal tx_data  : std_logic_vector(9 downto 0) := (others => '1');
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: tinyTX_example PORT MAP (
          clk => clk,
          switch => sw,
          JOY_UP => btnC,
          tx => RsTx,
          led => led
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	
	-- Clock for 19200 baud rate
	process(clk) 
    begin
        if rising_edge(clk) then
            if count = 100_000_000/19_200-1 then
                count <= (others => '0');
            else
                count <= count +1;
            end if;
        end if;
    end process;
	 with count select baud_clock <= '1' when "0000000000000", '0' when others;
	 
	-- Counter for Ascii table
	process(clk)
	begin
			if rising_edge(clk) and led(0) = '0' then
				if ascii_count = x"7A" then
					ascii_count <= x"41";
				else
					ascii_count <= ascii_count + 1;
				end if;
			end if;
	end process;
	sw <= std_logic_vector(ascii_count);
	 
	-- Capture serial output
	process(clk, baud_clock, led(0))
		variable l : line;
	begin
		if falling_edge(baud_clock) and led(0) = '1' then
			tx_data <= RsTx & tx_data(9 downto 1);
		end if;
		-- Write the captured output to console
		if falling_edge(led(0)) and btnC = '1' then
			hwrite (l, tx_data(8 downto 1));
			writeline (output, l);
		end if;
	end process;

   -- Stimulus process
   stim_proc: process
		variable l : line;
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for clk_period*10;

      -- insert stimulus here 
		wait for 468.545 us;
		--sw <= x"65";
		btnC <= '1';
		--wait for 500 us;
		--btnC <= '0';
		
		--wait for 1.1 ms;
		
		--write (l, String'("Hello world!"));
--		hwrite (l, tx_data);
--      writeline (output, l);

      wait;
   end process;

END;
