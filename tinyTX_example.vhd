----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Mike Field
-- 			 Jack Gassett
-- Create Date:    July 2016
-- Design Name: 
-- Module Name:    TinyTX - http://hamsterworks.co.nz/mediawiki/index.php/TinyTx
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--	
-- This example is meant to be used with the LogicStart MegaWing
-- 
-- Set the character you want to send out the serial port using the switches.
-- Then press joystick up to send the character out over the serial port.
-- LED(0) shows when the tinyTX module is busy.
--
-- The module is hard coded to 19200 bps serial speed. 
-- 
-- Switch to simulation and run the testbench to see the core in action.
--
-- The generated bit file can be cound in the ise_work directory.
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity tinyTX_example is
    Port ( clk  : in STD_LOGIC;
           SWITCH   : in STD_LOGIC_VECTOR (7 downto 0);
           JOY_UP : in STD_LOGIC;
           TX : out STD_LOGIC;
           LED  : out STD_LOGIC_VECTOR (7 downto 0) := (others => '1'));
end tinyTX_example;

architecture Behavioral of tinyTX_example is
    component tiny_rs232_tx is
    Port ( clk         : in  STD_LOGIC;
           data        : in  STD_LOGIC_VECTOR(7 downto 0);
           data_enable : in  STD_LOGIC;
           bit_tick    : in  STD_LOGIC;
           tx          : out STD_LOGIC := '1';
           busy        : out STD_LOGIC);
    end component;
    signal count      : unsigned(12 downto 0)        := (others => '1');
    signal bit_tick   : std_logic := '0';
begin
    with count select bit_tick <= '1' when "0000000000000", '0' when others;

process(clk) 
    begin
        if rising_edge(clk) then
            if count = 100_000_000/19_200-1 then
                count <= (others => '0');
            else
                count <= count +1;
            end if;
        end if;
    end process;

i_tiny_rs232_tx: tiny_rs232_tx port map (
    clk         => clk,
    data        => SWITCH,
    data_enable => JOY_UP,
    bit_tick    => bit_tick,
    tx          => TX,
    busy        => LED(0));

end Behavioral;