----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Mike Field
-- 
-- Create Date:    July 2016
-- Design Name: 
-- Module Name:    TinyTX - http://hamsterworks.co.nz/mediawiki/index.php/TinyTx
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity tiny_rs232_tx is
    Port ( clk         : in  STD_LOGIC;
           bit_tick    : in  STD_LOGIC;
           data        : in  STD_LOGIC_VECTOR(7 downto 0);
           data_enable : in  STD_LOGIC;
           tx          : out STD_LOGIC := '1';
           busy        : out STD_LOGIC );
end tiny_rs232_tx;

architecture Behavioral of tiny_rs232_tx is
    signal shift_reg  : std_logic_vector(9 downto 0) := (others => '1');
    signal i_busy     : std_logic;
begin
    busy <= i_busy;
    with shift_reg select i_busy <= '0' when "0000000000", '1' when others;

clk_proc: process(clk)
    begin
        if rising_edge(clk) then
            if i_busy = '0' and data_enable = '1' then
                shift_reg <= '1' & data & '0';
            end if;             
        
            if bit_tick = '1' then
                if i_busy = '1' then
                    tx <= shift_reg(0);
                    shift_reg <= '0' & shift_reg(9 downto 1);
                end if;
            end if;
        end if;
    end process;
end Behavioral;
